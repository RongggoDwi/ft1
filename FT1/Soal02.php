<?php 

// Toko 1: 0.5 km 0.5 --> jarak gudang
// + 1.5 + 1.5 + 1.5
// Toko 2: 2 km
// Toko 3: 3.5 km
// Toko 4: 5 km

$perjalanan = readline("Perjalanan Ke : ");
$strPerjalanan = strtoupper($perjalanan);

$arrPerjalanan = explode("-",$strPerjalanan);
$jarak = 0;
$waktu = 0;

for ($i=0; $i < count($arrPerjalanan); $i++) { 
    if ($arrPerjalanan[$i] == "1") {
        if ($jarak > 0) {
            if ($arrPerjalanan[$i - 1] == "4") {
                $jarak = $jarak + 5;
                $waktu += 10;
            }
            else if ($arrPerjalanan[$i - 1] == "3") {
                $jarak = $jarak + 3;
                $waktu += 10;
            }
            else if ($arrPerjalanan[$i - 1] == "2") {
                $jarak = $jarak + 1.5;
                $waktu += 10;
            }
            else if ($arrPerjalanan[$i - 1] == "1") {
                $jarak = $jarak + 0.5;
                $waktu += 10;
            }
        }
    }
    else if ($arrPerjalanan[$i] == "2") {
        if ($arrPerjalanan[$i - 1] == "1") {
            $jarak = $jarak + 1.5;
            $waktu += 10;
        }
        else if ($arrPerjalanan[$i - 1] == "2") {
            $jarak = $jarak + 0;
            $waktu += 10;
        }
        else if ($arrPerjalanan[$i - 1] == "3") {
            $jarak = $jarak + 1.5;
            $waktu += 10;
        }
        else if ($arrPerjalanan[$i - 1] == "4") {
            $jarak = $jarak + 3;
            $waktu += 10;
        }
    }
    else if ($arrPerjalanan[$i] == "3") {
        if ($arrPerjalanan[$i - 1] == "2") {
            $jarak = $jarak + 1.5;
            $waktu += 10;
        }
        else if ($arrPerjalanan[$i - 1] == "1") {
            $jarak = $jarak + 3.5;
            $waktu += 10;
        }
        else if ($arrPerjalanan[$i - 1] == "3") {
            $jarak = $jarak + 0;
            $waktu += 10;
        }
        else if ($arrPerjalanan[$i - 1] == "4") {
            $jarak = $jarak + 1.5;
            $waktu += 10;
        }
    }
    else if ($arrPerjalanan[$i] == "4") {
        if ($arrPerjalanan[$i - 1] == "2") {
            $jarak = $jarak + 3;
            $waktu += 10;
        }
        else if ($arrPerjalanan[$i - 1] == "1") {
            $jarak = $jarak + 5;
            $waktu += 10;
        }
        else if ($arrPerjalanan[$i - 1] == "3") {
            $jarak = $jarak + 1.5;
            $waktu += 10;
        }
        else if ($arrPerjalanan[$i - 1] == "4") {
            $jarak = $jarak + 0;
            $waktu += 10;
        }
    }
}

$kecepatan = $jarak * 0.5;
$waktu = $jarak / $kecepatan;

echo $waktu." menit";
echo "\n";
echo "Jarak total : ".$jarak." km";

?>